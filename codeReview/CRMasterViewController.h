//
//  CRMasterViewController.h
//  codeReview
//
//  Created by Nguyen Huynh on 4/29/14.
//  Copyright (c) 2014 Nguyen Huynh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRDetailViewController;

#import <CoreData/CoreData.h>

@interface CRMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) CRDetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
