//
//  CRAppDelegate.h
//  codeReview
//
//  Created by Nguyen Huynh on 4/29/14.
//  Copyright (c) 2014 Nguyen Huynh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
