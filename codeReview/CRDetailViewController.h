//
//  CRDetailViewController.h
//  codeReview
//
//  Created by Nguyen Huynh on 4/29/14.
//  Copyright (c) 2014 Nguyen Huynh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
