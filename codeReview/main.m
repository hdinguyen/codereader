//
//  main.m
//  codeReview
//
//  Created by Nguyen Huynh on 4/29/14.
//  Copyright (c) 2014 Nguyen Huynh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CRAppDelegate class]));
    }
}
